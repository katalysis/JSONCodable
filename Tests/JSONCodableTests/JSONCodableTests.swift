import XCTest
@testable import JSONCodable

class JSONCodableTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(JSONCodable().text, "Hello, World!")
    }


    static var allTests : [(String, (JSONCodableTests) -> () throws -> Void)] {
        return [
            ("testExample", testExample),
        ]
    }
}
