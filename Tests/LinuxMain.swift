import XCTest
@testable import JSONCodableTestSuite

XCTMain([
     testCase(JSONCodableTests.allTests),
])
